function output=Quat_inverse(input)
q=input(1:4);

q_inv(1)= q(1)/(q(1)*q(1)+q(2:4)'*q(2:4));
q_inv(2:4) = -1*q(2:4)/(q(1)*q(1)+q(2:4)'*q(2:4));
output=[q_inv];
end