function output=Quat_product(input)
q1=input(1:4);
q2=input(5:8);
q_prod(1)= q1(1)*q2(1)-q1(2:4)'*q2(2:4);
q_prod(2:4) = q1(1)*q2(2:4)+q2(1)*q1(2:4)+skew(q1(2:4))*q2(2:4);
output=[q_prod];
end