function output=Jacob_inverse(input)
q_dot = input(1:7);
Jacob = [input(8:13) input(14:19) input(20:25) input(26:31) input(32:37) input(38:43) input(44:49)];
M_inv = [input(50:56) input(57:63) input(64:70) input(71:77) input(78:84) input(85:91) input(92:98)];


Jacob_inv = M_inv*Jacob'*pinv(Jacob*M_inv*Jacob',0.01);

output=[Jacob_inv];