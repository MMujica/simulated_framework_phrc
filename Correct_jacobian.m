function output=Correct_jacobian(input)
x_ee = input(1:6);
Jacob = [input(7:12) input(13:18) input(19:24) input(25:30) input(31:36) input(37:42) input(43:48)];
Jacob = [Jacob(4:6,:);Jacob(1:3,:)]; % Jacobian from matlab toolbox is first angular, then linear, so i invert that


output=[Jacob];