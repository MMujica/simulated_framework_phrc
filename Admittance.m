function output=Impedance(input)
global Kx;global Dx;global Md;

x_ee = input(1:6); x_ee_dot = input(7:12); x_ref = input(13:18); x_ref_dot = input(19:24);
Jacob = [input(25:30) input(31:36) input(37:42) input(43:48) input(49:54) input(55:60) input(61:66)];
Q_e = input(67:70); Q_ref = input(71:74);
M = [input(75:81) input(82:88) input(89:95) input(96:102) input(103:109) input(110:116) input(117:123)];
Jacob_dot = [input(124:129) input(130:135) input(136:141) input(142:147) input(148:153) input(154:159) input(160:165)];
q_dot = input(166:172);
x_ref_dot_dot = input(173:178);
Cq_dot = input(179:185);
he = input(186:191);
grav = input(192:198);


error = x_ref-x_ee;
error_dot=x_ref_dot-x_ee_dot;
%%%%
Q_diff = Quat_product([Q_ref; Quat_inverse(Q_e)']);

error(4:6) = Q_diff(2:4);
E = Q_diff(1)*eye(3) - skew(Q_diff(2:4));
Kor =2*E'*Kx(4:6,4:6);
Kx2=Kx;
Kx2(4:6,4:6)=Kor;

M_inv = inv(M);
Jacob_inv = M_inv*Jacob'*pinv(Jacob*M_inv*Jacob',0.01);
u_imp = Jacob_inv*inv(Md)*(Md*x_ref_dot_dot+Dx*(x_ref_dot-x_ee_dot)+Kx2*(error)-Md*Jacob_dot*q_dot-he);
u = M*u_imp + Cq_dot + grav + Jacob'*he ;


output=[u];