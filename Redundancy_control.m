    function output=Redundancy_control(input)
global kns; global iiwa;
q_dot = input(1:7);
Jacob = [input(8:13) input(14:19) input(20:25) input(26:31) input(32:37) input(38:43) input(44:49)];
M_inv = [input(50:56) input(57:63) input(64:70) input(71:77) input(78:84) input(85:91) input(92:98)];
M = [input(99:105) input(106:112) input(113:119) input(120:126) input(127:133) input(134:140) input(141:147)];
x_ee = input(147:152);
q = input(153:159);

u_ns = - kns*(q_dot);

Jacob_inv1 = M_inv*Jacob'*pinv(Jacob*M_inv*Jacob',0.01);
tau_R = (eye(7)-Jacob'*Jacob_inv1')*u_ns;

output = [tau_R];

