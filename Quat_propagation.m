function output=Quat_propagation(input)
w = input(1:3);
eta=input(4);
epsilon=input(5:7);

E = eta*eye(3) - skew(epsilon);
eta_dot = -0.5*epsilon'*w;
epsilon_dot = 0.5*E*w;
output=[eta_dot epsilon_dot'];
end