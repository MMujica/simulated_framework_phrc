function output=K_orientation(input)
global Kx;
eta=input(1);
epsilon=input(2:4);
E = eta*eye(3) - skew(epsilon);
output=[2*E'*Kx(4:6,4:6)*epsilon];
end