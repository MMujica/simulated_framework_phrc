global Kx; global Dx;global Md;
global kns; global Kh; global Kpt; global Kdt;
%admittance dynamic 2

Md = 10*eye(6,6);
Md(4:6,4:6) = 1*eye(3,3);
Kx = 0*eye(6,6);
Kx(1:3,1:3) = zeros(3,3);
Dx = 20*eye(6,6);
Dx(4:6,4:6) = 2*eye(3,3);
Kh = 176*eye(6,6);
Dh = 19*eye(6,6);
kns = 5;

% Motion control
Kpt = 200*eye(6,6);
Kdt = 100*eye(6,6);

% Load
Load = 0.0

%%%%%%%%%%%
global iiwa;
iiwa = importrobot('iiwa14.urdf');
iiwa.DataFormat = 'column';
iiwa.Gravity = [0 0 -9.81];

% Add box at the ee
box = robotics.RigidBody('iiwa_box');
joint_box = robotics.Joint('joint_box','fixed');
setFixedTransform(joint_box,trvec2tform([-0.00 -0.00 0.00]))
box.Joint = joint_box;
box.Mass = 0.941;
box.CenterOfMass = [0 0 0.15];
box.Inertia = [0.0238+box.Mass*box.CenterOfMass(3) 0.0176+box.Mass*box.CenterOfMass(3) 0.0106 0 0 0];
tf_visual_box = eye(4,4);
tf_visual_box(1:3,4) = [-0.0 -0.0 0.15]
tf_visual_box(1:3,1:3)=roty(90)*tf_visual_box(1:3,1:3);
addVisual(box,"Mesh",'BOX_2.stl',tf_visual_box)
addBody(iiwa,box,'iiwa_link_ee_kuka');
% Add sensor at the eend ob box
sensor = robotics.RigidBody('iiwa_sensor');
joint_sensor = robotics.Joint('joint_sensor','fixed');
%setFixedTransform(joint_sensor,trvec2tform([0 -0.00 0.3])) %box
setFixedTransform(joint_sensor,trvec2tform([0 -0.00 0.3-0.035+0.061])) %box - long matlab flange + real flange
sensor.Joint = joint_sensor;
sensor.Mass = 0.47;
sensor.CenterOfMass = [0 0 0.028];
sensor.Inertia = [0.000662+sensor.Mass*sensor.CenterOfMass(3) 0.000662+sensor.Mass*sensor.CenterOfMass(3) 0.000376 0 0 0];
tf_visual_sensor = eye(4,4);
tf_visual_sensor(1:3,4) = [-0.0 -0.0 0.056/2]
tf_visual_sensor(1:3,1:3)=roty(90)*tf_visual_sensor(1:3,1:3);
addVisual(sensor,"Mesh",'CYL_2.stl',tf_visual_sensor)
addBody(iiwa,sensor,'iiwa_box');
%Add new end effector at the end
iiwa_new_ee = robotics.RigidBody('iiwa_new_ee');
joint_iiwa_new_ee = robotics.Joint('joint_new_ee','fixed');
setFixedTransform(joint_iiwa_new_ee,trvec2tform([0.06 0.00 0.056]))
iiwa_new_ee.Joint = joint_iiwa_new_ee;
iiwa_new_ee.Mass = 0.0;
iiwa_new_ee.CenterOfMass = [0 0 0];
iiwa_new_ee.Inertia = [0.0 0 0 0 0 0];
addBody(iiwa,iiwa_new_ee,'iiwa_sensor');
% 
% iiwa_new_ee = robotics.RigidBody('iiwa_new_ee');
% joint_iiwa_new_ee = robotics.Joint('joint_new_ee','fixed');
% setFixedTransform(joint_iiwa_new_ee,trvec2tform([0 -0.00 0.3]))
% iiwa_new_ee.Joint = joint_iiwa_new_ee;
% iiwa_new_ee.Mass = 0.0;
% iiwa_new_ee.CenterOfMass = [0 0 0];
% iiwa_new_ee.Inertia = [0.0 0 0 0 0 0];
% addBody(iiwa,iiwa_new_ee,'iiwa_box');

% Create the real iiwa with extra load
iiwa_real = copy(iiwa);
iiwa_box_load = copy(getBody(iiwa,'iiwa_box'))
iiwa_box_load.Mass = iiwa_box_load.Mass+Load;
iiwa_box_load.Inertia = [0.0238+iiwa_box_load.Mass*iiwa_box_load.CenterOfMass(3) 0.0176+iiwa_box_load.Mass*iiwa_box_load.CenterOfMass(3) 0.0106 0 0 0];
replaceBody(iiwa_real,'iiwa_box',iiwa_box_load);


q_initial = [0 deg2rad(56.12) 0 deg2rad(-56.76) 0 deg2rad(-23.64) 0];
%q_initial=[-0.000812 0.980352 0.000055 -1.010951 0.000677 -0.410969 -0.000077]
transform = getTransform(iiwa,q_initial',"iiwa_new_ee") %
xd_initial_pos = [transform(1,4),transform(2,4), transform(3,4)]
xd_initial_or2 = rotm2quat(transform(1:3,1:3));
xd_initial_or = xd_initial_or2(2:4);


tstart=0;
tfinal=2.5;
initial_pose = [xd_initial_pos xd_initial_or] % box with sensor
final_pose = initial_pose + [-0.0 0.0 0.25 -0.0 -0.0 0.0]
for i = 1:6
    [a5,a4,a3,a2,a1,a0] = createTraj5(initial_pose(i),final_pose(i),0,0,0,0,tstart,tfinal)
    % make a polynomial
    p = [a5,a4,a3,a2,a1,a0];
    % Create time vector
    t1 = linspace(tstart,tfinal,tfinal/0.001);
    % Evaluate the polynomial : Position
    xref_h1(:,i) = polyval(p,t1);
    % calculate the first derivative : Velocity
    pd = polyder(p);
    % Evaluate the velocity
    dxref_h1(:,i) = polyval(pd,t1);
    % calculate the second derivative : Acceleration
    pdd = polyder(pd);
    % Evaluate the acceleration
    ddxref_h1(:,i) = polyval(pdd,t1);
end
%initial_pose(3)=initial_pose(3)+0.03 % To make final point different from
%initial
for i = 1:6
    [a5,a4,a3,a2,a1,a0] = createTraj5(final_pose(i),initial_pose(i),0,0,0,0,tstart,tfinal)
    % make a polynomial
    p = [a5,a4,a3,a2,a1,a0];
    % Create time vector
    t2 = linspace(tstart,tfinal,tfinal/0.001);
    % Evaluate the polynomial : Position
    xref_h2(:,i) = polyval(p,t2);
    % calculate the first derivative : Velocity
    pd = polyder(p);
    % Evaluate the velocity
    dxref_h2(:,i) = polyval(pd,t2);
    % calculate the second derivative : Acceleration
    pdd = polyder(pd);
    % Evaluate the acceleration
    ddxref_h2(:,i) = polyval(pdd,t2);
end
pause_time=600;
pause_time2=150;

t_traj = [0:0.001:4*tfinal-0.001];
xref_h = [t_traj' [repmat(xref_h1(1,:),pause_time,1);xref_h1;repmat(xref_h1(end,:),pause_time2,1);xref_h2;repmat(xref_h2(end,:),length(t_traj)-5000-pause_time-pause_time2,1)] ];
dxref_h = [t_traj' [zeros(pause_time,6);dxref_h1;zeros(pause_time2,6);dxref_h2;zeros(length(t_traj)-5000-pause_time-pause_time2,6)] ];
ddxref_h = [t_traj' [zeros(pause_time,6);ddxref_h1;zeros(pause_time2,6);ddxref_h2;zeros(length(t_traj)-5000-pause_time-pause_time2,6)] ];
