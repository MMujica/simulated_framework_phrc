function output=Orientation(input)
R=input;

r11=R(1,1);r12=R(1,2);r13=R(1,3);
r21=R(2,1);r22=R(2,2);r23=R(2,3);
r31=R(3,1);r32=R(3,2);r33=R(3,3);


eta=0.5*sqrt(r11+r22+r33+1);


epsilon=0.5*[mysign(r32-r23)*sqrt(r11-r22-r33+1);
            mysign(r13-r31)*sqrt(r22-r33-r11+1);
            mysign(r21-r12)*sqrt(r33-r11-r22+1)];

output=[eta epsilon'];

end