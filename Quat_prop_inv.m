function output=Quat_prop_inv(input)
eta=input(1);
epsilon=input(2:4);
eta_dot=input(5);
epsilon_dot=input(6:8);

E = eta*eye(3) - skew(epsilon);

w=2*inv(E)*epsilon_dot
output=[w];
end